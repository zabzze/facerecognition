import face_recognition
import math
from fastapi import FastAPI
import urllib
import urllib.request

app = FastAPI()

# calculate accuracy percentage of face
def face_confidence(face_distance, face_match_threshold=0.6):
    range = (1.0 - face_match_threshold)
    linear_val = (1.0 - face_distance) / (range * 2.0)

    if face_distance > face_match_threshold:
        return str(round(linear_val * 100, 2)) + '%'
    else:
        value = (linear_val + ((1.0 - linear_val) * math.pow((linear_val - 0.5) * 2, 0.2))) * 100
        return str(round(value, 2)) + '%'
    

class FaceRecognition:
    path_face_front = None
    path_passport = None
    known_face_encoding = None

    def __init__(self, path_face_front, path_passport):
        self.path_face_front = path_face_front
        self.path_passport = path_passport
        self.encode_face()

    def is_url(self, url):
        return urllib.parse.urlparse(url).scheme != ""
    
    def encode_face(self):
        image = self.path_face_front
        if self.is_url(self.path_face_front):
            image = urllib.request.urlopen(self.path_face_front)
        face_image = face_recognition.load_image_file(image)

        locations = face_recognition.face_locations(face_image)
        if len(locations) != 1:
            raise Exception("No unique face detected!")

        self.known_face_encoding = face_recognition.face_encodings(face_image)[0]

    # todo: recognition with more photos additional to passport?
    def run_recognition(self):
        image = self.path_passport
        if self.is_url(self.path_passport):
            image = urllib.request.urlopen(self.path_passport)
        passport_image = face_recognition.load_image_file(image)
        # find all faces in image
        locations = face_recognition.face_locations(passport_image)
        if len(locations) != 1:
            raise Exception("No unique face detected!")
        
        passport_encoding = face_recognition.face_encodings(passport_image, locations)[0]        
        face_distance = face_recognition.face_distance([self.known_face_encoding], passport_encoding)[0]

        return face_confidence(face_distance)
    
    
        

@app.get("/face-comp")
def doit(path_face: str, path_passport: str):
    fr = FaceRecognition(path_face, path_passport)
    return fr.run_recognition()


# start of main method
# __name__ == '__main__' is done to make sure that this module does not get executed if it's imported by another file,
# it gets executed only if this file is run directly! 
if __name__ == '__main__': 
    # path_face = 'faces/Sabrina.jpg'
    # path_passport = 'testimages/Sabrina_Pass.jpg'
    path_face = 'https://upload.wikimedia.org/wikipedia/commons/c/c2/Rihanna_Fenty_2018.png'
    path_passwort = 'https://pyxis.nymag.com/v1/imgs/175/e54/88d30a8689b681d64f857efde2ddf7b472-rihanna.rsquare.w330.jpg'
    fr = FaceRecognition(path_face, path_passwort)
    print(fr.run_recognition())