import requests
import io

arguments = {
    'path_face': 'https://upload.wikimedia.org/wikipedia/commons/c/c2/Rihanna_Fenty_2018.png',
    'path_passport': 'https://pyxis.nymag.com/v1/imgs/175/e54/88d30a8689b681d64f857efde2ddf7b472-rihanna.rsquare.w330.jpg'
}
resp = requests.get("http://127.0.0.1:8000/face-comp", params=arguments)
print(f"Code: {resp.status_code}, Result: {resp.text}")