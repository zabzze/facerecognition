import face_recognition
import os
import numpy as np
import math


# calculate accuracy percentage of face
def face_confidence(face_distance, face_match_threshold=0.6):
    range = (1.0 - face_match_threshold)
    linear_val = (1.0 - face_distance) / (range * 2.0)

    if face_distance > face_match_threshold:
        return str(round(linear_val * 100, 2)) + '%'
    else:
        value = (linear_val + ((1.0 - linear_val) * math.pow((linear_val - 0.5) * 2, 0.2))) * 100
        return str(round(value, 2)) + '%'
    

class FaceRecognition:
    face_locations = []
    face_encodings = []
    face_names = []
    known_face_encodings = []
    known_face_names = []

    def __init__(self):
        self.encode_faces()

    def encode_faces(self):
        for image in os.listdir('faces'):
            face_image = face_recognition.load_image_file(f'faces/{image}')
            face_encoding = face_recognition.face_encodings(face_image)[0]

            self.known_face_encodings.append(face_encoding)
            self.known_face_names.append(image)
        print(self.known_face_names)

    def run_recognition(self):
        for image in sorted(os.listdir('testimages')):
            face_image = face_recognition.load_image_file(f'testimages/{image}')
            # find all faces in image
            self.face_locations = face_recognition.face_locations(face_image)
            self.face_encodings = face_recognition.face_encodings(face_image, self.face_locations)        

            self.face_names = []
            for face_encoding in self.face_encodings:
                matches = face_recognition.compare_faces(self.known_face_encodings, face_encoding)
                
                name = 'Unknown'
                confidence = 'Unknown'

                face_distances = face_recognition.face_distance(self.known_face_encodings, face_encoding)
                best_match_index = np.argmin(face_distances)

                if matches[best_match_index]:
                    name = self.known_face_names[best_match_index]
                    confidence = face_confidence(face_distances[best_match_index])
                    self.face_names.append(f'{image}: {name} ({confidence})')
            
            print(self.face_names)
        


# start of main method
# __name__ == '__main__' is done to make sure that this module does not get executed if it's imported by another file,
# it gets executed only if this file is run directly! 
if __name__ == '__main__': 
    fr = FaceRecognition()
    fr.run_recognition()